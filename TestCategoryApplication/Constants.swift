//
//  Constants.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

let IdKey = "id"
let TitleKey = "title"
let CreatedKey = "created_at"
let UpdatedKey = "updated_at"
let SlugKey = "slug"
let BodyKey = "body"
let PageCategoryIdKey = "page_category_id"

let CategorySubcategorySegueId = "CategorySubcategorySegue"
let SubcategoryPostSegueId = "SubcategoryPostSegue"
