//
//  LoadService.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import Foundation

typealias LoadCompletionBlock = (Data?, URLResponse?, Error?) -> Void

class LoadService{
    func getDataWithUrl(urlString: String, complection: @escaping LoadCompletionBlock){
        let url = URL(string: urlString)
        let downloadTask = URLSession.shared.dataTask(with: url!, completionHandler: complection)
        downloadTask.resume()
    }
}
