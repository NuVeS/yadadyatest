//
//  SubcategoryVC.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubcategoryVC: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!{
        didSet{
            mainTableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
            mainTableView.rowHeight = UITableViewAutomaticDimension
            mainTableView.estimatedRowHeight = 44
        }
    }
    
    var serviceController: ServiceController?
    var categoryId: String?
    var items: JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Posts"
        loadData()
    }
    
    func loadData(){
        serviceController?.loadSubcategoryListWithId(id: categoryId!, andCompletion: { (items, error) in
            if let itemList = items{
                self.items = itemList
                DispatchQueue.main.async {
                    self.mainTableView.reloadData()
                }
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else {
            return
        }
        
        if segueId == SubcategoryPostSegueId{
            let row = mainTableView.indexPathForSelectedRow?.row
            
            let postVC = segue.destination as? PostVC
            postVC?.categoryId = self.categoryId
            postVC?.subcategoryId = items[row!][IdKey].stringValue
            postVC?.serviceController = self.serviceController
            mainTableView.deselectRow(at: IndexPath(row: row!, section: 0), animated: true)
        }
    }

}

extension SubcategoryVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? PostTableViewCell
        
        cell?.titleLabel?.text = items[indexPath.row][TitleKey].stringValue
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: SubcategoryPostSegueId, sender: nil)
    }
}
