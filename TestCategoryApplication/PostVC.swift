//
//  PostVC.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostVC: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!{
        didSet{
            mainTableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
            mainTableView.rowHeight = UITableViewAutomaticDimension
            mainTableView.estimatedRowHeight = 44
        }
    }
    
    var serviceController: ServiceController?
    var items: JSON = []
    var categoryId: String?
    var subcategoryId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Detail"
        loadData()
    }
    
    func loadData(){
        serviceController?.loadPostWithCategoryId(id: categoryId!, subcategoryId: subcategoryId!, andCompletion: { (items, error) in
            if let itemList = items{
                self.items = itemList
                DispatchQueue.main.async {
                    self.mainTableView.reloadData()
                }
            }
        })
    }
}

extension PostVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? PostTableViewCell
        
        cell?.titleLabel?.text = self.setTextForCellAtIndexPathRow(row: indexPath.row)
        
        return cell!
    }
    
    func setTextForCellAtIndexPathRow(row: Int) -> String{
        switch row {
        case 0:
            return items[TitleKey].stringValue
        case 1:
            return items[BodyKey].stringValue
        case 2:
            return items[SlugKey].stringValue
        case 3:
            return items[CreatedKey].stringValue
        case 4:
            return items[UpdatedKey].stringValue
        default:
            return "";
        }
    }
}
