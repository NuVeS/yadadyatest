//
//  ParseService.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ParseCompletionBlock = (JSON?, Error?)->Void

class ParseService{
    func parseData(data:Data, completion: ParseCompletionBlock){
        var error: NSError? = NSError()
        let object = JSON(data: data, options: [], error: &error)

        if object == JSON.null{
            completion(nil, error)
        }else{
            completion(object, nil)
        }
    }
}
