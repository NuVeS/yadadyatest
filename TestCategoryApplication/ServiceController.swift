//
//  ServiceController.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ServeCompletionBlock = (JSON?, Error?)->Void

class ServiceController{
    var loadService: LoadService
    var parseService: ParseService
    
    init() {
        loadService = LoadService()
        parseService = ParseService()
    }
    
    func loadCategoryListWithCompletion(completion: @escaping ServeCompletionBlock){
        let urlString = CategoryURL
        loadService.getDataWithUrl(urlString: urlString) { (data, _, error) in
            self.didLoadData(data: data, withCompletion: completion, andError: error)
        }
    }
    
    func loadSubcategoryListWithId(id: String, andCompletion completion: @escaping ServeCompletionBlock){
        let urlString = String(format: SubcategoryURL, id)
        loadService.getDataWithUrl(urlString: urlString) { (data, _, error) in
            self.didLoadData(data: data, withCompletion: completion, andError: error)
        }
    }
    
    func loadPostWithCategoryId(id: String, subcategoryId: String, andCompletion completion: @escaping ServeCompletionBlock){
        let urlString = String(format: PostURL, id, subcategoryId)
        loadService.getDataWithUrl(urlString: urlString) { (data, _, error) in
            self.didLoadData(data: data, withCompletion: completion, andError: error)
        }
    }
    
    func didLoadData(data: Data?, withCompletion completion: ServeCompletionBlock, andError error: Error?){
        if error != nil{
            completion(nil, error)
        }else{
            self.parseService.parseData(data: data!, completion: completion)
        }
    }
    
}
