//
//  MainNavigationVC.swift
//  TestCategoryApplication
//
//  Created by Максуд on 10.03.17.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit

class MainNavigationVC: UINavigationController {

    let serviceController = ServiceController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.injectServiceController()
    }
    
    func injectServiceController(){
        let rootVC = self.viewControllers.first as? CategoryViewController
        rootVC?.serviceController = self.serviceController
    }


}
